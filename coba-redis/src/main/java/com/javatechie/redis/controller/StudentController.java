package com.javatechie.redis.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.javatechie.redis.entity.Student;
import com.javatechie.redis.repository.StudentRepository;

@RestController
public class StudentController {

	@Autowired
    private StudentRepository studentRepository;
	
	@PostMapping("/student")
    public void saveStudent(@RequestBody Student student) {
        studentRepository.save(student);
    }
	
	@GetMapping("/student/{id}")
    public Student getStudent(@PathVariable String id) {
        return studentRepository.findById(id);
    }

    @GetMapping("/student")
    public Map<String, Student> getStudents() {
        return studentRepository.findAll();
    }

    @PutMapping("/student")
    public void updateStudent(@RequestBody Student student) {
        studentRepository.update(student);
    }

    @DeleteMapping("/student/{id}")
    public void deleteStudent(@PathVariable String id) {
        studentRepository.delete(id);
    }
	
	
}

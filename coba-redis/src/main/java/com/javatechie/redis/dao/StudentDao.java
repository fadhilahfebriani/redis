package com.javatechie.redis.dao;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import com.javatechie.redis.entity.Student;
import com.javatechie.redis.repository.StudentRepository;

import jakarta.annotation.PostConstruct;

@Repository
public class StudentDao implements StudentRepository {

	private static final String KEY = "STUDENT";
	
	@Autowired
	private RedisTemplate<String, Object> redisTemplate;
	
	private HashOperations<String, String, Student> hashOperations;
	
	@PostConstruct
	private void init() {
		hashOperations = redisTemplate.opsForHash();
	}
	
	@Override
	public void save(Student student) {
		hashOperations.put(KEY, student.getId(), student);
	}
	
	@Override
	public Student findById(String id) {
		return hashOperations.get(KEY, id);
	}
	
	@Override
	public Map<String, Student> findAll() {
		return hashOperations.entries(KEY);
	}
	
	@Override
	public void update(Student student) {
		save(student);
	}
	
	@Override
	public void delete(String id) {
		hashOperations.delete(KEY, id);
	}
	
	
	
}

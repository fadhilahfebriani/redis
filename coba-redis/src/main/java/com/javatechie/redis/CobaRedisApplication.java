package com.javatechie.redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CobaRedisApplication {

	public static void main(String[] args) {
		SpringApplication.run(CobaRedisApplication.class, args);
	}

}

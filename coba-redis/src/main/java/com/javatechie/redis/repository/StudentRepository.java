package com.javatechie.redis.repository;

import java.util.Map;

import org.springframework.boot.autoconfigure.security.SecurityProperties.User;

import com.javatechie.redis.entity.Student;

public interface StudentRepository {

	void save(Student student);
	Student findById(String id);
	Map<String, Student> findAll();
	void update(Student student);
	void delete(String id);
}
